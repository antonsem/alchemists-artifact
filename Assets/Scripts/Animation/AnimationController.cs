using ExtraTools;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimationController : MonoBehaviour
{
    [SerializeField]
    private Animator anim;
    [SerializeField]
    private AudioClip[] stepSounds;

    private int walkHash = Animator.StringToHash("Walk");
    private int jumpHash = Animator.StringToHash("Jump");
    private int groundedHash = Animator.StringToHash("Grounded");
    private int hangHash = Animator.StringToHash("Hang");
    private int dashHash = Animator.StringToHash("Dash");
    private int attackHash = Animator.StringToHash("Attack");
    private int deathHash = Animator.StringToHash("Death");

    private bool dashState = false;
    private bool hangState = false;
    private bool groundedState = false;
    private bool deathState = false;

    public void SetAttack()
    {
        anim.SetTrigger(attackHash);
    }

    public void SetDeath(bool state)
    {
        if(deathState != state)
        {
            deathState = state;
            anim.SetBool(deathHash, state);
        }
    }

    public void SetDash(bool state)
    {
        if (dashState != state)
        {
            dashState = state;
            anim.SetBool(dashHash, state);
        }
    }

    public void SetWalk(float speed)
    {
        anim.SetFloat(walkHash, Mathf.Abs(speed));
    }

    public void SetJump(float speed)
    {
        anim.SetFloat(jumpHash, speed);
    }

    public void SetHang(bool state)
    {
        if (hangState != state)
        {
            hangState = state;
            anim.SetBool(hangHash, state);
        }
    }

    public void SetGrounded(bool state)
    {
        if (groundedState != state)
        {
            groundedState = state;
            anim.SetBool(groundedHash, state);
        }
    }

    public void PlayStep()
    {
        AudioSource.PlayClipAtPoint(stepSounds[Random.Range(0, stepSounds.Length - 1)], transform.position);
    }

    private void Reset()
    {
        transform.EnsureComponent(out anim);
    }
}
