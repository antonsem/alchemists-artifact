using ExtraTools;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class DoorAnimationController : MonoBehaviour
{
    [SerializeField]
    private Animator anim;

    private int activateHash = Animator.StringToHash("Activate");
    private bool currentState = false;

    public void SetActive(bool state)
    {
        if (currentState != state)
        {
            currentState = state;
            anim.SetBool(activateHash, state);
        }
    }

    private void Reset()
    {
        transform.EnsureComponent(out anim);
    }
}
