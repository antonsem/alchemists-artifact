using ExtraTools;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class DroneAnimationController : MonoBehaviour
{
    [SerializeField]
    private Animator anim;
    private int deathHash = Animator.StringToHash("Death");
    private int aggressiveHash = Animator.StringToHash("Aggressive");

    private bool aggressionState = false;

    [SerializeField]
    private AudioClip explosion;

    public void SetDeath()
    {
        anim.SetTrigger(deathHash);
        AudioSource.PlayClipAtPoint(explosion, transform.position);
    }

    public void SetAggression(bool state)
    {
        if (state != aggressionState)
        {
            aggressionState = state;
            anim.SetBool(aggressiveHash, state);
        }
    }

    private void Reset()
    {
        transform.EnsureComponent(out anim);
    }
}
