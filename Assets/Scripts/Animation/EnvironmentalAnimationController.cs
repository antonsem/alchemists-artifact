using ExtraTools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class EnvironmentalAnimationController : MonoBehaviour
{
    [SerializeField]
    private Animator anim;

    private int actionHash = Animator.StringToHash("Action");

    public void SetAction()
    {
        anim.SetTrigger(actionHash);
    }

    private void Reset()
    {
        transform.EnsureComponent(out anim);
    }
}
