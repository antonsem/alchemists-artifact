using UnityEngine;

namespace Constants
{
    public static class Variables
    {
        public static readonly int backgroundPos = 1;
        public static readonly int foregroundPos = 0;
    }

    public static class Tags
    {
        public static readonly string reloadable = "Reloadable";
    }

    public static class Layers
    {
        public static readonly int player = LayerMask.NameToLayer("Player");
    }
}
