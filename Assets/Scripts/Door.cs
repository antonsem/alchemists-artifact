using ExtraTools;
using UnityEngine;

[RequireComponent(typeof(DoorAnimationController))]
public class Door : MonoBehaviour
{
    public int id = -1;
    public int mateId = -1;
    public GameObject currentScene;
    public GameObject nextScene;
    public Layer layer;
    [SerializeField]
    private DoorAnimationController anim;
    private bool canExit = false;
    public bool tempCheck = false;

    public Vector3 Enter()
    {
        ObjectHolder.Instance.Player.Teleport(transform.position + new Vector3(0, 0.1f, 0));
        Events.Instance.enteredThroughDoor.Invoke(this);
        Events.Instance.layerChangedTo.Invoke(layer.layerTag);
        return transform.position + new Vector3(0, 0.1f, 0);
    }

    public void Exit()
    {
        if (currentScene && nextScene && mateId >= 0)
            SceneChanger.Instance.ChangeScene(currentScene, nextScene, mateId);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (currentScene && nextScene && mateId >= 0 && tempCheck)
        {
            anim.SetActive(true);
            canExit = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (currentScene && nextScene && mateId >= 0)
        {
            anim.SetActive(false);
            canExit = false;
        }
    }

    private void Reset()
    {
        transform.EnsureComponent(out anim);
        layer = transform.GetComponentInParent<Layer>();
        if (!layer)
            Debug.LogError(string.Format("{0} did not find any Layer component in its parents!", name));
        currentScene = transform.GetComponentInParent<Room>().gameObject;
        if (!currentScene)
            Debug.LogError(string.Format("{0} did not find any Room component in its parents!", name));

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q) && canExit)
        {
            canExit = false;
            Exit();
        }
    }
}
