using ExtraTools;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(DroneAnimationController))]
public class Drone : Patrol, IBreakable
{
    [SerializeField]
    private float defaultHealth = 1;
    public float Health { get; private set; }
    [SerializeField]
    private float dashSpeed = 3;
    [SerializeField]
    private DroneAnimationController anim;
    [SerializeField]
    private Transform flipObj;
    [SerializeField]
    private GameObject colliders;
    [SerializeField]
    private float checkTime = 1;
    private float defaultCheckTime = 1;
    [SerializeField]
    private Transform eyes;
    [SerializeField]
    private float sight = 5;
    [SerializeField]
    private LayerMask visionMask;
    private Ray ray = new Ray();
    private RaycastHit hit;
    private Vector3 dashDir;

    [SerializeField]
    private GameObject[] disableOnDeath;

    private List<Vector3> patrolPoints = new List<Vector3>();

    private Vector3 right = new Vector3(0, 180, 0);
    private Vector3 left = new Vector3(0, 0, 0);

    private bool seesPlayer = false;

    public void OnHit(float damage)
    {
        if (--Health <= 0)
            Die();
    }

    private void Die()
    {
        anim.SetDeath();
        if (disableOnDeath != null)
        {
            for (int i = 0; i < disableOnDeath.Length; i++)
                disableOnDeath[i].SetActive(false);
        }
        Health = 0;
    }

    private void Disable()
    {
        gameObject.SetActive(false);
    }

    private bool LookForPlayer()
    {
        Vector3 playerPos = ObjectHolder.Instance.Player.transform.position;
        Vector3 dir = playerPos - transform.position;
        dir.z = 0;
        dir.Normalize();

        ray.origin = eyes.position;
        ray.direction = dir;

        Debug.DrawLine(ray.origin, ray.direction * sight, Color.red, 1);

        if (Physics.Raycast(ray, out hit, sight, visionMask, QueryTriggerInteraction.Ignore))
        {
            dashDir = dir;
            return hit.transform.gameObject.layer == Constants.Layers.player;
        }
        else
            return false;
    }

    private void Dash()
    {
        phys.targetVelocity = dashDir * dashSpeed;
    }

    public void Hit(ref Collider col)
    {
        if (seesPlayer)
        {
            if (col.attachedRigidbody && col.attachedRigidbody.transform.SetComponent(out IBreakable breakale))
                breakale.OnHit(5);

            Die();
        }
    }

    private void Awake()
    {
        defaultCheckTime = checkTime;
    }

    private void OnEnable()
    {
        seesPlayer = false;
        Health = defaultHealth;
        colliders.SetActive(true);
        if (disableOnDeath != null)
        {
            for (int i = 0; i < disableOnDeath.Length; i++)
                disableOnDeath[i].SetActive(true);
        }
    }

    protected override void Update()
    {
        if (Health <= 0) return;

        if (seesPlayer)
        {
            Dash();
            return;
        }

        if (checkTime >= 0)
            checkTime -= Time.deltaTime;
        else
        {
            checkTime = defaultCheckTime;
            seesPlayer = LookForPlayer();
        }

        anim.SetAggression(seesPlayer);

        if (seesPlayer)
        {
            colliders.SetActive(false);
            return;
        }

        base.Update();

        if (flipObj)
        {
            if (phys.targetVelocity.x > 0.01f)
                flipObj.rotation = Quaternion.Euler(right);
            else if (phys.targetVelocity.x < -0.01f)
                flipObj.rotation = Quaternion.Euler(left);
        }
    }

    private void Reset()
    {
        transform.EnsureComponent(out anim);
        transform.EnsureComponent(out phys);
    }


}
