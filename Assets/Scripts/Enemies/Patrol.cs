using ExtraTools;
using UnityEngine;

[RequireComponent(typeof(PhysicsObject))]
public class Patrol : MonoBehaviour
{
    [SerializeField]
    private Transform[] waypoints;
    private int wpIndex = 0;

    [SerializeField]
    protected PhysicsObject phys;
    [SerializeField]
    protected float speed = 3;

    [SerializeField]
    protected float reachDistSqrd = 0.01f;

    private void SelectWaypoint()
    {
        if (++wpIndex >= waypoints.Length)
            wpIndex = 0;
    }

    protected virtual void Update()
    {
        if (waypoints == null || waypoints.Length == 0) return;
        if (Vector3.SqrMagnitude(transform.position - waypoints[wpIndex].position) < reachDistSqrd)
            SelectWaypoint();

        phys.targetVelocity = (waypoints[wpIndex].position - transform.position).normalized * speed;
    }

    private void Reset()
    {
        transform.EnsureComponent(out phys);
    }
}
