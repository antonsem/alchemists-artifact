using UnityEngine;
using TMPro;

public class GemsUI : MonoBehaviour
{
    [SerializeField]
    private PickupCountHolder holder;
    [SerializeField]
    private TextMeshProUGUI count;
    [SerializeField]

    private void Awake()
    {
        Events.Instance.refreshUI.AddListener(Refresh);
    }

    private void Refresh()
    {
        count.text = string.Format("Gems Collected: {0} / {1}", holder.count, holder.total);
    }
}
