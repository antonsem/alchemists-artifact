using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GroundBrush", menuName = "Alchemy/Ground Brush")]  
[CustomGridBrush(false, true, false, "Alchemy/Ground Brush")]
public class GroundBrush : GridBrushBase
{
    public GameObject prefab;

    public int zPos = 0;

    public override void Paint(GridLayout gridLayout, GameObject brushTarget, Vector3Int position)
    {
        Vector3Int cellPos = new Vector3Int(position.x, position.y, zPos);

        GameObject go = Instantiate(prefab, brushTarget.transform);
        go.transform.position = gridLayout.LocalToWorld(gridLayout.CellToLocalInterpolated(cellPos + Vector3.one * 0.5f));
    }
}
