using ExtraTools;
using UnityEngine;

public class Hazard : MonoBehaviour
{
    [SerializeField]
    private int hit = 1;

    private void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody && other.attachedRigidbody.transform.SetComponent(out IBreakable breakable))
            breakable.OnHit(hit);
    }
}
