using UnityEngine;

public class HitDetector : MonoBehaviour, IBreakable
{
    public void OnHit(float damage)
    {
        Events.Instance.gotHit.Invoke(this, damage);
    }
}
