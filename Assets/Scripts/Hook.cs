using ExtraTools;
using UnityEngine;

public class Hook : MonoBehaviour
{
    [SerializeField]
    private TriggerCheck handTop;
    [SerializeField]
    private TriggerCheck handBottom;
    [SerializeField]
    private Collider hook;
    [SerializeField]
    private Player player;//TODO: crappy temporary solution - Anton

    public float disableForSeconds = 0;

    public bool IsHooked => hook.enabled;
    public bool IsSliding => handBottom.isColliding && handTop.isColliding;

    private void Update()
    {
        if (!player.canHang)
        {
            hook.enabled = false;
            return;
        }
        if(disableForSeconds > 0)
        {
            disableForSeconds -= Time.deltaTime;
            hook.enabled = false;
        }
        else if (handBottom.isColliding && !handTop.isColliding)
            hook.enabled = true;
        else if (!handBottom.isColliding || handTop.isColliding)
            hook.enabled = false;
    }

    private void Reset()
    {
        transform.EnsureComponent(out hook);
    }
}
