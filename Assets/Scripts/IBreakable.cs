public interface IBreakable
{
    void OnHit(float damage);
}
