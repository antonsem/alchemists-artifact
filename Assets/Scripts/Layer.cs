using Constants;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Layer : MonoBehaviour
{
    public LayerTag layerTag;
    public int unit = 10;
    public int moveSpeed = 5;
    public int vMax = 0;
    public int vMin = 0;
    public int hMax = 0;
    public int hMin = 0;

    [SerializeField]
    private List<GameObject> enableOnReload = new List<GameObject>();

    private IEnumerator move;

    public void MoveVertical(int ammount, Action doneCallback = null, Action failCallback = null)
    {
        int nextPos = Mathf.RoundToInt(transform.position.y + ammount * unit);
        if (move != null || nextPos > vMax || nextPos < vMin)
        {
            failCallback?.Invoke();
            return;
        }

        Vector3 newPos = transform.position;
        newPos.y = nextPos;
        move = MoveCoroutine(newPos, doneCallback);
        StartCoroutine(move);
    }

    public void MoveHorizontal(int ammount, Action doneCallback = null, Action failCallback = null)
    {
        int nextPos = Mathf.RoundToInt(transform.position.x + ammount * unit);
        if (move != null || nextPos > hMax || nextPos < hMin)
        {
            failCallback?.Invoke();
            return;
        }

        Vector3 newPos = transform.position;
        newPos.x = nextPos;
        move = MoveCoroutine(newPos, doneCallback);
        StartCoroutine(move);
    }

    private IEnumerator MoveCoroutine(Vector3 newPos, Action callback = null)
    {
        while (Vector3.SqrMagnitude(transform.position - newPos) > 0.001f)
        {
            transform.position = Vector3.MoveTowards(transform.position, newPos, Time.deltaTime * moveSpeed);
            yield return null;
        }

        transform.position = newPos;

        move = null;
        callback?.Invoke();
    }

    public void Reload()
    {
        transform.position = layerTag == LayerTag.Background ? new Vector3(0, 0, 1) : Vector3.zero;
        if (enableOnReload != null)
            for (int i = 0; i < enableOnReload.Count; i++)
                enableOnReload[i].SetActive(true);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
            MoveVertical(1);
        if (Input.GetKeyDown(KeyCode.J))
            MoveVertical(-1);
        if (Input.GetKeyDown(KeyCode.H))
            MoveHorizontal(-1);
        if (Input.GetKeyDown(KeyCode.K))
            MoveHorizontal(1);
    }

    #region Inspector
#if UNITY_EDITOR
    private void Reset()
    {
        SearchForReloadable();
    }

    [ExecuteInEditMode]
    private void ClearReloadable()
    {
        enableOnReload.Clear();
    }

    [ExecuteInEditMode]
    private void SearchForReloadable()
    {
        foreach (Transform go in transform.GetComponentsInChildren<Transform>())
        {
            if (go.tag == Tags.reloadable && !enableOnReload.Contains(go.gameObject))
                enableOnReload.Add(go.gameObject);
        }

        //UnityEditor.PropertyModification[] mod = new UnityEditor.PropertyModification[1];
        //mod[0] = new UnityEditor.PropertyModification();
        //mod[0].objectReference = this;
        //UnityEditor.PrefabUtility.SetPropertyModifications(gameObject, mod);
    }

    [Space(15), Header("Inspector"), Space(5), SerializeField, InspectorButton("ClearReloadable")]
    private string clearReloadable;
    [SerializeField, InspectorButton("SearchForReloadable")]
    private string searchForReloadable;

#endif
    #endregion
}
