using ExtraTools;
using UnityEngine;

[RequireComponent(typeof(EnvironmentalAnimationController))]
public class LayerLever : MonoBehaviour
{
    [SerializeField]
    private Layer layerToOperate;
    [SerializeField]
    private EnvironmentalAnimationController anim;
    [SerializeField]
    private GameObject useTip;
    [SerializeField]
    private GameObject moveTip;

    private bool canOperate = false;
    private bool operating = false;
    private bool moved = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == ObjectHolder.Instance.Player.gameObject.layer)
        {
            canOperate = true;
            useTip.SetActive(true);
            moveTip.SetActive(false);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == ObjectHolder.Instance.Player.gameObject.layer)
        {
            canOperate = false;
            if (operating)
            {
                ObjectHolder.Instance.Player.canMove = true;
                operating = moved = false;
            }

            moveTip.SetActive(false);
            useTip.SetActive(false);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q) && canOperate)
        {
            operating = !operating;
            ObjectHolder.Instance.Player.canMove = !operating;
            useTip.SetActive(!operating);
            moveTip.SetActive(operating);
        }

        if (operating && !moved)
        {
            if (Input.GetAxisRaw("Horizontal") > 0.1f)
            {
                if (!moved)
                {
                    moved = true;
                    anim.SetAction();
                }
                layerToOperate.MoveHorizontal(1, () => { moved = false; }, () => { moved = false; });
            }
            else if ((Input.GetAxisRaw("Horizontal") < -0.1f))
            {
                if (!moved)
                {
                    moved = true;
                    anim.SetAction();
                }
                layerToOperate.MoveHorizontal(-1, () => { moved = false; }, () => { moved = false; });
            }
            else if (Input.GetAxisRaw("Vertical") > 0.1f)
            {
                if (!moved)
                {
                    moved = true;
                    anim.SetAction();
                }
                layerToOperate.MoveVertical(1, () => { moved = false; }, () => { moved = false; });
            }
            else if ((Input.GetAxisRaw("Vertical") < -0.1f))
            {
                if (!moved)
                {
                    moved = true;
                    anim.SetAction();
                }
                layerToOperate.MoveVertical(-1, () => { moved = false; }, () => { moved = false; });
            }
        }
    }

    private void Reset()
    {
        transform.EnsureComponent(out anim);
    }
}
