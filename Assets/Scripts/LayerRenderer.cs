using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LayerRenderer : MonoBehaviour
{
    [SerializeField]
    private float colorChangeSpeed = 5;
    [SerializeField]
    private RawImage foreground;
    [SerializeField]
    private RawImage background;

    [SerializeField]
    private Color foregroundInactive;
    [SerializeField]
    private Color foregroundActive;
    [SerializeField]
    private Color backgroundInactive;
    [SerializeField]
    private Color backgroundActive;

    private IEnumerator colorChange;
    private bool quitting = false;

    private void Start()
    {
        ChangeCurrentLayer(StateHolder.Instance.currentLayer);
    }

    private void OnEnable()
    {
        Events.Instance.layerChangedTo.AddListener(ChangeCurrentLayer);
    }

    private void OnDisable()
    {
        if (!quitting)
            Events.Instance.layerChangedTo.RemoveListener(ChangeCurrentLayer);
    }

    private void OnApplicationQuit()
    {
        quitting = true;
    }

    private IEnumerator ChangeColorCoroutine(RawImage backgroundImage, RawImage foregroundImage, Color backgroundColor, Color foregroundColor)
    {
        while(backgroundImage.color != backgroundColor
            || foregroundImage.color != foregroundColor)
        {
            backgroundImage.color = Color.Lerp(backgroundImage.color, backgroundColor, Time.deltaTime * colorChangeSpeed);
            foregroundImage.color = Color.Lerp(foregroundImage.color, foregroundColor, Time.deltaTime * colorChangeSpeed);
            yield return null;
        }

        colorChange = null;
    }

    private void ChangeCurrentLayer(LayerTag newLayer)
    {
        if (colorChange != null)
            StopCoroutine(colorChange);

        if (newLayer == LayerTag.Background)
            colorChange = ChangeColorCoroutine(background, foreground, backgroundActive, foregroundInactive);
        else if (newLayer == LayerTag.Foreground)
            colorChange = ChangeColorCoroutine(background, foreground, backgroundInactive, foregroundActive);
        else
            Debug.LogError(string.Format("Color format is not set for {0} layer", newLayer.ToString()));

        if (colorChange != null)
            StartCoroutine(colorChange);

    }
}
