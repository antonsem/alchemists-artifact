using ExtraTools;
using System;
using UnityEngine;

[RequireComponent(typeof(Collider), typeof(EnvironmentalAnimationController))]
public class LayerSwitch : MonoBehaviour
{
    [SerializeField]
    private LayerTag toLayer;
    [SerializeField]
    private LayerMask mask;
    [SerializeField]
    private Color available = Color.green;
    [SerializeField]
    private Color unavailable = Color.red;
    [SerializeField]
    private SpriteRenderer indicatorImage;

    private bool canSwitch = false;
    [SerializeField]
    private bool _hasLayer = false;
    private bool HasLayer
    {
        get => _hasLayer;
        set
        {
            if (_hasLayer != value)
            {
                _hasLayer = value;
                indicatorImage.color = _hasLayer ? available : unavailable;
            }
        }
    }
    private Ray ray;

    [SerializeField]
    private EnvironmentalAnimationController anim;
    [SerializeField]
    private GameObject tip;

    public bool Activate(GameObject obj)
    {
        if (!HasLayer)
            return false;
        if (toLayer == LayerTag.Foreground)
        {
            if (obj.transform.SetComponent(out Rigidbody rigid))
            {
                try
                {
                    rigid.position = new Vector3(obj.transform.position.x, obj.transform.position.y + 0.1f, Constants.Variables.foregroundPos);
                    return true;
                }
                catch (Exception e)
                {
                    Debug.LogError(string.Format("Something wrong happend when trying to change to layer {0}!\nERROR\n{1}", toLayer.ToString(), e.ToString()));
                    return false;
                }
            }
            else
            {
                try
                {
                    obj.transform.position = new Vector3(obj.transform.position.x, obj.transform.position.y + 0.1f, Constants.Variables.foregroundPos);
                    return true;
                }
                catch (Exception e)
                {
                    Debug.LogError(string.Format("Something wrong happend when trying to change to layer {0}!\nERROR\n{1}", toLayer.ToString(), e.ToString()));
                    return false;
                }
            }
        }
        else if (toLayer == LayerTag.Background)
        {
            if (obj.transform.SetComponent(out Rigidbody rigid))
            {
                try
                {
                    rigid.position = new Vector3(obj.transform.position.x, obj.transform.position.y + 0.05f, Constants.Variables.backgroundPos);
                    return true;
                }
                catch (Exception e)
                {
                    Debug.LogError(string.Format("Something wrong happend when trying to change to layer {0}!\nERROR\n{1}", toLayer.ToString(), e.ToString()));
                    return false;
                }
            }
            else
            {
                try
                {
                    obj.transform.position = new Vector3(obj.transform.position.x, obj.transform.position.y + 0.05f, Constants.Variables.backgroundPos);
                    return true;
                }
                catch (Exception e)
                {
                    Debug.LogError(string.Format("Something wrong happend when trying to change to layer {0}!\nERROR\n{1}", toLayer.ToString(), e.ToString()));
                    return false;
                }
            }
        }
        else
        {
            Debug.LogError(string.Format("Transfer to layer {0} is not set!", toLayer.ToString()));
            return false;
        }
    }
    #region Mono

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == ObjectHolder.Instance.Player.gameObject.layer)//dirty solution
        {
            canSwitch = true;
            tip.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == ObjectHolder.Instance.Player.gameObject.layer)
        {
            canSwitch = false;
            tip.SetActive(false);
        }
    }

    private void Reset()
    {
        transform.EnsureComponent(out Collider col);
        transform.EnsureComponent(out Collider anim);

        col.isTrigger = true;
    }

    private void Start()
    {
        ray = new Ray(transform.position, toLayer == LayerTag.Background ? Vector3.forward : -Vector3.forward);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q) && canSwitch)
        {
            if (Activate(ObjectHolder.Instance.Player.gameObject))
            {
                Events.Instance.layerChangedTo.Invoke(toLayer);
                anim.SetAction();
            }
            canSwitch = false;
        }
    }

    private void FixedUpdate()
    {
        ray.origin = transform.position;
        HasLayer = Physics.Raycast(ray, 2, mask, QueryTriggerInteraction.Collide);
    }
    #endregion
}
