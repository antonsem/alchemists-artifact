using ExtraTools;
using UnityEngine;

public class ObjectHolder : Singleton<ObjectHolder>
{
    [SerializeField]
    private Player player;
    public Player Player
    {
        get
        {
            if (!player)
                Utils.FindOfType(out player, true);
            return player;
        }
    }

    private void Reset()
    {
        Utils.FindOfType(out player, true);
    }
}
