using ExtraTools;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PhysicsObject : MonoBehaviour
{
    [SerializeField]
    private float minValY = 0.5f;
    [SerializeField]
    private float gravity = 20;
    public float terminalVelocity = -25;
    public Vector3 targetVelocity;
    public bool isGrounded = false;
    public bool useGravity = true;
    public bool forceVerticalVelocity = false;

    [SerializeField]
    private Rigidbody rigid;

    private Vector3 _velocity;
    public Vector3 Velocity => _velocity;
    private float minDist = 0.01f;
    private RaycastHit[] hits;
    private Vector3 groundNormal = Vector3.up;

    [SerializeField]
    private bool debug = false;

    private void Move(Vector3 move, bool yMovement)
    {
        float dist = move.magnitude;
        if (dist <= minDist) return;
        hits = rigid.SweepTestAll(move, dist, QueryTriggerInteraction.Ignore);
        for (int i = 0; i < hits.Length; i++)
        {
            Vector3 norm = hits[i].normal;
            if (debug)
                Debug.DrawRay(hits[i].point, norm, Color.red, 3);

            if (norm.y > minValY)
            {
                isGrounded = true;
                if (yMovement)
                {
                    groundNormal = norm;
                    norm.x = 0;
                }
            }

            float projection = Vector3.Dot(_velocity, norm);
            if (projection < 0)
                _velocity -= projection * norm;

            float modifiedDist = hits[i].distance - minDist;
            if (modifiedDist < dist)
            {
                dist = modifiedDist;
                if (debug)
                    Debug.DrawRay(hits[i].point, -hits[i].normal, Color.green, 3);
            }
        }

        rigid.position += move.normalized * dist;
    }

    public void Teleport(Vector3 pos)
    {
        if (!isActiveAndEnabled)
            transform.position = pos;
        else
            rigid.position = pos;
    }

    #region Mono
    private void Awake()
    {
        if (!rigid)
        {
            Debug.LogWarning(string.Format("Rigidbody component of {0} is not set! Searching for one...", name));
            if (!transform.SetComponent(out rigid, true))
            {
                enabled = false;
                return;
            }
        }
    }

    private void FixedUpdate()
    {
        if (targetVelocity.y != 0 || forceVerticalVelocity)
        {
            _velocity.y = targetVelocity.y;
            forceVerticalVelocity = false;
        }
        if (useGravity)
            _velocity += gravity * Physics.gravity * Time.fixedDeltaTime;

        if (_velocity.y < terminalVelocity)
            _velocity.y = terminalVelocity;
        _velocity.x = targetVelocity.x;
        isGrounded = false;
        Vector3 deltaPos = Velocity * Time.fixedDeltaTime;

        Vector3 moveAlongGround = new Vector3(groundNormal.y + Mathf.Abs(groundNormal.x), 0);
        Vector3 move = moveAlongGround * deltaPos.x;
        Move(move, false);
        move = Vector3.up * deltaPos.y;

        Move(move, true);

        targetVelocity = Vector3.zero;
    }

    private void Reset()
    {
        transform.EnsureComponent(out rigid);
    }
    #endregion
}
