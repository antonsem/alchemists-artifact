using Constants;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == Layers.player)
        {
            Events.Instance.gotPickup.Invoke(this);
            gameObject.SetActive(false);
        }
    }
}
