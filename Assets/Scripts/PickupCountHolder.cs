using UnityEngine;

[CreateAssetMenu(fileName = "PickupCountHolder", menuName = "AA/Pickup Count Holder")]
public class PickupCountHolder : ScriptableObject
{
    public int count = 0;
    public int total = 0;
}
