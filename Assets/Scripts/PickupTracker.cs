using UnityEngine;

public class PickupTracker : MonoBehaviour
{
    [SerializeField]
    private Pickup[] pickups;

    [SerializeField]
    private PickupCountHolder holder;

    private void Awake()
    {
        Events.Instance.gotPickup.AddListener(GotPickup);
        ResetAll();
    }

    private void Start()
    {
        Events.Instance.refreshUI.Invoke();
    }

    private void GotPickup(Pickup pickup)
    {
        if (++holder.count >= pickups.Length)
            Events.Instance.gotAllPickups.Invoke();
        Events.Instance.refreshUI.Invoke();
    }

    private void ResetAll()
    {
        for (int i = 0; i < pickups.Length; i++)
            pickups[i].gameObject.SetActive(true);

        holder.count = 0;
        holder.total = pickups.Length;
        Events.Instance.refreshUI.Invoke();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Backspace))
            ResetAll();
    }
}
