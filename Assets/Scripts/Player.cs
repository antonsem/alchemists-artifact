using ExtraTools;
using UnityEngine;

[RequireComponent(typeof(PhysicsObject), typeof(AnimationController))]
public class Player : MonoBehaviour, IBreakable
{
    #region Stats
    public int health = 1;
    public float moveSpeed = 7.5f;
    public float jumpSpeed = 20;
    public int jumpCount = 1;
    public float slideSpeed = -0.01f;
    public float slideJumpSpeedLock = 0.15f;
    public float slideJumpVerticalMultiplier = 0.75f;
    public float sprintMultiplier = 1.5f;
    public float dashSpeed = 50;
    public float dashTime = 0.1f;
    private int defaultJumpCount = 1;

    public bool canMove = true;
    public bool canSlide = false;
    public bool canHang = false;
    public bool canSprint = false;
    public bool canDash = false;
    public bool canHit = false;
    #endregion

    #region Assignables
    [Header("Assignables")]
    [SerializeField]
    private Hook hook;
    [SerializeField]
    private Transform flipObj;
    [SerializeField]
    private PhysicsObject phys;
    [SerializeField]
    private AnimationController anim;
    #endregion

    private Vector3 right = new Vector3(0, 0, 0);
    private Vector3 left = new Vector3(0, 180, 0);
    private float hSpeedLock = 0;
    private float vSpeedLock = 0;
    private float prevHSpeed = 0;
    private float prevVSpeed = 0;

    private float jumpCooldown = 0;//used to solve double jump bug when holding on a ledge

    private bool dashing = false;
    private bool attacking = false;

    private void GetInput()
    {
        if (hSpeedLock > 0)
            phys.targetVelocity.x = prevHSpeed;

        if (vSpeedLock > 0)
            phys.targetVelocity.y = prevVSpeed;
        else
            phys.useGravity = true;

        if (hSpeedLock <= 0)
        {
            dashing = false;
            phys.targetVelocity.x = Input.GetAxisRaw("Horizontal") * moveSpeed *
                (Input.GetKey(KeyCode.LeftShift) && canSprint ? sprintMultiplier : 1);
        }

        if (Input.GetAxisRaw("Vertical") < 0)
            hook.disableForSeconds = 0.1f;
        else if (canSlide && hook && hook.IsSliding && phys.Velocity.y <= 0 && phys.targetVelocity.y == 0)
            phys.targetVelocity.y = slideSpeed;

        if (Input.GetKeyDown(KeyCode.LeftControl) && canDash)
        {
            phys.targetVelocity.y = 0;
            phys.targetVelocity.x = flipObj.right.x * dashSpeed;
            phys.useGravity = false;
            phys.forceVerticalVelocity = dashing = true;
            vSpeedLock = hSpeedLock = dashTime;
        }

        if (vSpeedLock <= 0 && Input.GetKeyDown(KeyCode.Space)
            && (jumpCount > 0 || (hook && hook.IsSliding)))
        {
            jumpCount--;
            jumpCooldown = 0.15f;
            if (!hook || !hook.IsSliding || phys.isGrounded)
                phys.targetVelocity.y = jumpSpeed;
            else if (canSlide && hook && hook.IsSliding)
            {
                phys.targetVelocity.y = jumpSpeed * slideJumpVerticalMultiplier;
                phys.targetVelocity.x = -flipObj.right.x * jumpSpeed;
                hSpeedLock = slideJumpSpeedLock;
            }
        }

        if (Input.GetKeyDown(KeyCode.E) && canHit)
            attacking = true;

        prevHSpeed = phys.targetVelocity.x;
        prevVSpeed = phys.targetVelocity.y;
    }

    private void SetAnimations()
    {
        if (flipObj)
        {
            if (phys.targetVelocity.x > 0.01f)
                flipObj.rotation = Quaternion.Euler(right);
            else if (phys.targetVelocity.x < -0.01f)
                flipObj.rotation = Quaternion.Euler(left);
        }

        if (health <= 0)
        {
            anim.SetDeath(true);
            return;
        }

        anim.SetDash(dashing);
        if (dashing) return;

        bool hanging = hook && ((canHang && hook.IsHooked) || (hook.IsSliding && !phys.isGrounded && canSlide));

        anim.SetHang(hanging);
        if (hook && hook.IsHooked && !phys.isGrounded)
            return;
        if (attacking)
        {
            if (!hanging)
                anim.SetAttack();
            attacking = false;
        }

        anim.SetGrounded(phys.isGrounded);

        if (phys.isGrounded)
        {
            anim.SetWalk(phys.Velocity.x);
        }
        else
        {
            anim.SetWalk(0);
            anim.SetJump(phys.Velocity.y / -phys.terminalVelocity);
        }
    }

    public void Teleport(Vector3 pos)
    {
        phys.Teleport(pos);
    }

    private void Die()
    {
        Debug.LogError("Player is dead!");
        canMove = false;
    }

    public void Resurrect()
    {
        canMove = true;
        health = 1;
        anim.SetDeath(false);
    }

    public void OnHit(float damage)
    {
        if (--health <= 0)
            Die();
    }

    #region Mono
    private void Start()
    {
        defaultJumpCount = jumpCount;
    }

    private void Update()
    {
        if (canMove)
            GetInput();
        SetAnimations();
        if (phys.isGrounded && phys.targetVelocity.y <= 0 && jumpCooldown <= 0)
            jumpCount = defaultJumpCount;
        if (jumpCooldown > 0)
            jumpCooldown -= Time.deltaTime;
    }

    private void FixedUpdate()
    {
        if (vSpeedLock > 0)
            vSpeedLock -= Time.fixedDeltaTime;
        if (hSpeedLock > 0)
            hSpeedLock -= Time.fixedDeltaTime;
    }

    private void Reset()
    {
        transform.EnsureComponent(out phys);
        transform.EnsureComponent(out anim);
    }
    #endregion
}
