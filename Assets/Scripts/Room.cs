using UnityEngine;

public class Room : MonoBehaviour
{
    [SerializeField]
    private Layer[] layers;
    [SerializeField]
    private Door entrance;
    [SerializeField]
    private Cinemachine.CinemachineVirtualCamera cam;

    private bool isQuitting = false;

    public void ReloadRoom()
    {
        for (int i = 0; i < layers.Length; i++)
            layers[i].Reload();

        ObjectHolder.Instance.Player.Teleport(entrance.transform.position + Vector3.up * 0.1f);
        ObjectHolder.Instance.Player.Resurrect();
        Events.Instance.layerChangedTo.Invoke(entrance.layer.layerTag);
    }

    private void EnteredThroughDoor(Door door)
    {
        entrance = door;
        cam.Follow = ObjectHolder.Instance.Player.transform;
    }

    #region Mono
    private void OnEnable()
    {
        Events.Instance.enteredThroughDoor.AddListener(EnteredThroughDoor);
    }

    private void OnDisable()
    {
        if (!isQuitting)
            Events.Instance.enteredThroughDoor.RemoveListener(EnteredThroughDoor);
    }

    private void OnApplicationQuit()
    {
        isQuitting = true;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
            ReloadRoom();
    }
    #endregion

    #region Inspector
#if UNITY_EDITOR
    [Space(25), Header("Inspector"), Space(5), SerializeField, InspectorButton("ReloadLayers")]
    private string reloadLayers;

    private void ReloadLayers()
    {
        layers = transform.GetComponentsInChildren<Layer>();
    }

    private void Reset()
    {
        ReloadLayers();
    }
#endif
    #endregion
}
