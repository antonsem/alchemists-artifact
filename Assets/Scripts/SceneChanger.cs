using System;
using System.Collections;
using UnityEngine;

public class SceneChanger : Singleton<SceneChanger>
{
    private Door[] doors;
    private WaitForSeconds wait = new WaitForSeconds(0.1f);
    private GameObject newRoomObj;

    public void ChangeScene(GameObject oldRoom, GameObject newRoom, int doorId)
    {
        StartCoroutine(ChangeSceneCoroutine(oldRoom, newRoom, doorId));
    }

    private IEnumerator ChangeSceneCoroutine(GameObject oldRoom, GameObject newRoom, int doorId)
    {
        ObjectHolder.Instance.Player.canMove = false;

        if (oldRoom)
            oldRoom.SetActive(false);

        if (newRoom)
            newRoomObj = Instantiate(newRoom);
        yield return null;
        if (oldRoom)
            Destroy(oldRoom);
        yield return null;
        if (newRoom)
        {
            doors = newRoom.GetComponentsInChildren<Door>();
            Vector3 doorPos = ObjectHolder.Instance.Player.transform.position;
            for (int i = 0; i < doors.Length; i++)
            {
                if (doors[i].id == doorId)
                {
                    doorPos = doors[i].Enter();
                    break;
                }
            }
            yield return null;
            GC.Collect();
            yield return wait;
            //while (ObjectHolder.Instance.Player.transform.position != doorPos)
            //{
            //    ObjectHolder.Instance.Player.Teleport(doorPos);
            //    yield return null;
            //}
            if (!ObjectHolder.Instance.Player.isActiveAndEnabled)
                ObjectHolder.Instance.Player.gameObject.SetActive(true);
            ObjectHolder.Instance.Player.canMove = true;
        }
    }
}
