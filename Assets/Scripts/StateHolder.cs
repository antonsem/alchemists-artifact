using UnityEngine.SceneManagement;

public enum LayerTag
{
    Foreground,
    Background
}

public class StateHolder : Singleton<StateHolder>
{
    public Scene currentScene;
    public LayerTag currentLayer;

    private bool quitting = false;

    private void OnEnable()
    {
        Events.Instance.layerChangedTo.AddListener(ChangeCurrentLayer);
    }

    private void OnDisable()
    {
        if (!quitting)
            Events.Instance.layerChangedTo.RemoveListener(ChangeCurrentLayer);
    }

    private void OnApplicationQuit()
    {
        quitting = true;
    }

    private void ChangeCurrentLayer(LayerTag newLayer)
    {
        currentLayer = newLayer;
    }
}
