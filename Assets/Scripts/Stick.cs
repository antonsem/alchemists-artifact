using ExtraTools;
using UnityEngine;

public class Stick : MonoBehaviour
{
    [SerializeField]
    private float enableTime = 0.25f;
    public float damage = 1;

    private void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody && other.attachedRigidbody.transform.SetComponent(out IBreakable breakable))
            breakable.OnHit(damage);
    }

    private void OnEnable()
    {
        enableTime = 0.25f;
    }

    private void Update()
    {
        if (enableTime <= 0)
            gameObject.SetActive(false);
        else
            enableTime -= Time.deltaTime;
    }
}
