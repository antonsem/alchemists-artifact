using UnityEngine;

public class TempDoorActivator : MonoBehaviour
{
    [SerializeField]
    private PickupCountHolder holder;
    [SerializeField]
    private Door door;

    private void Awake()
    {
        Events.Instance.refreshUI.AddListener(Check);
    }

    private void Check()
    {
        door.tempCheck = holder.count == holder.total;
    }

}
