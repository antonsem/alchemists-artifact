using UnityEngine;

public class TemporaryStart : MonoBehaviour
{
    public GameObject firstRoom;

    private void Start()
    {
        SceneChanger.Instance.ChangeScene(null, firstRoom, 0);
    }
}
