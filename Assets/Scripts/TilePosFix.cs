using UnityEngine;

public class TilePosFix : MonoBehaviour
{
    private void Start()
    {
        transform.position += transform.parent.position;
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.parent.position.z);

    }
}
