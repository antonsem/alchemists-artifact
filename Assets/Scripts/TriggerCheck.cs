using ExtraTools;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class TriggerCheck : MonoBehaviour
{
    public bool isColliding = false;
    public bool checkStay = true;

    public Collider col;

    [SerializeField]
    private Rigidbody rigid;

    private void Start()
    {
        rigid.isKinematic = true;
    }

    private void OnEnable()
    {
        isColliding = false;
    }

    private void OnDisable()
    {
        isColliding = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        isColliding = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (checkStay)
            isColliding = true;
    }

    private void OnTriggerExit(Collider other)
    {
        isColliding = false;
    }

    private void Reset()
    {
        transform.EnsureComponent(out col);
        transform.EnsureComponent(out rigid);
    }
}
