using UnityEngine;

[RequireComponent(typeof(Drone))]
public class TriggerHitDetector : MonoBehaviour
{
    [SerializeField]
    private Drone drone;

    private void OnTriggerEnter(Collider other)
    {
        drone.Hit(ref other);
    }
}
