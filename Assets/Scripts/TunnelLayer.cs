using ExtraTools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[RequireComponent(typeof(TilemapRenderer))]
public class TunnelLayer : MonoBehaviour
{
    [SerializeField]
    private TilemapRenderer rend;

    private void Awake()
    {
        rend.enabled = false;
    }

    private void Reset()
    {
        transform.EnsureComponent(out rend);
    }
}
