using UnityEngine.Events;

public class LayerEmmiter : UnityEvent<LayerTag>
{ }

public class DoorEmmiter : UnityEvent<Door>
{ }

public class HitEmmiter : UnityEvent<IBreakable, float>
{ }

public class PickupEmmiter : UnityEvent<Pickup>
{ }

public class EventEmmiter : UnityEvent
{ }

public class Events : Singleton<Events>
{
    public LayerEmmiter layerChangedTo = new LayerEmmiter();
    public DoorEmmiter enteredThroughDoor = new DoorEmmiter();
    public HitEmmiter gotHit = new HitEmmiter();
    public PickupEmmiter gotPickup = new PickupEmmiter();
    public EventEmmiter gotAllPickups = new EventEmmiter();
    public EventEmmiter refreshUI = new EventEmmiter();
}
